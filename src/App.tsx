import React, { Suspense } from 'react';
import { Container } from 'react-bootstrap';

const Main = React.lazy(() => import('./pages/Main'));

export default class App extends React.Component {

  public render(): JSX.Element {
    return (
      <Container className="app_container">
        <Suspense
          fallback={(
            <div className="d-flex align-items-center justify-content-center">
              <div className="spinner-border" role="projects_load" />
            </div>
          )}
        >
          <Main />
        </Suspense>
      </Container>
    );
  }
}