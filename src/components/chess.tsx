import React, { Component } from "react";
//import { Button } from 'react-bootstrap';
import Chessboard from "chessboardjsx";
import { ShortMove } from "chess.js";
import begin from "../public/begin.mp3";
//import gameover from "../public/gameover.mp3";
//import opponent_capture from "../public/opponent_capture.mp3";
//import opponent_castle from "../public/opponent_castle.mp3";
//import opponent_check from "../public/opponent.mp3";
import opponent_move from "../public/opponent_move.mp3";
import player_capture from "../public/player_capture.mp3";
//import player_castle from "../public/player_castle.mp3";
import player_check from "../public/player_check.mp3";
import player_move from "../public/player_move.mp3";
const Chess = require("chess.js");

interface ChessProps {
}

interface ChessState {
  fen: any,
  drag: boolean,
  orientation: any,
  status: string,
  moves: any,
  player_name: any,
  player_timer: any,
  opponent_name: any,
  opponent_timer: any,
  squareStyles: any
}

class ChessGame extends Component<ChessProps, ChessState> {

  chess = new Chess()
  chess_ws: WebSocket = null;

  light_square_styles = { backgroundColor: "#ecf5ff" } as React.CSSProperties;
  dark_square_styles = { backgroundColor: "#87a1b3" } as React.CSSProperties;
  highlight_square_styles = {
    boxShadow: "#296d98 0px 0px 1px 4px inset",
  } as React.CSSProperties;

  last_move_from_style = { backgroundColor: "#f6f669" } as React.CSSProperties;
  last_move_to_style = { backgroundColor: "#baca2b" } as React.CSSProperties;
  in_check_style = { backgroundColor: "red" } as React.CSSProperties;
  legal_moves_style = {
    backgroundColor: "rgba(0,0,0,.1)",
    backgroundClip: "content-box",
    borderRadius: "50%",
    padding: "35%",
  } as React.CSSProperties;
  take_styles = {
    boxShadow: "red 0px 0px 1px 4px inset",
  } as React.CSSProperties;
  take_and_last_move_styles = {
    backgroundColor: "#baca2b",
    boxShadow: "red 0px 0px 1px 4px inset",
  } as React.CSSProperties;

  constructor(props) {
    super(props)
    this.state = {
      fen: "start",
      drag: true,
      orientation: "white",
      status: "connecting",
      moves: [],
      player_name: "Guest",
      player_timer: 300,
      opponent_name: "Guest",
      opponent_timer: 300,
      squareStyles: {}
    }
  }

  connectSocket() {
    //this.chess_ws = new WebSocket("ws://25.64.141.174:8765")
    this.chess_ws = new WebSocket("ws://127.0.0.1:8765")
    this.chess_ws.onopen = () => {
      console.log("onopen");
      this.chess_ws.send(JSON.stringify({ name: "Sosna" }));
      this.setState({ player_name: "Sosna" })
      console.log("sent")
    }
    this.chess_ws.onmessage = (response) => {
      let msg = JSON.parse(response.data)
      console.log(msg)
      if (msg.type == "registration_result") {
        this.chess_ws.send(JSON.stringify({ type: "look_for_opponent" }));
        this.setState({ status: "waiting" })
      }
      if (msg.type == "begin_game") {
        this.setState({ orientation: msg.color, status: "start", drag: msg.color == "white" ? true : false, opponent_name: msg.opponent_name }, () => {
          this.play_sound(begin)
          if (msg.color == "black") {
            this.handleOpponentMove()
          }
        })
      }
    }
  }

  handleLocalMove = (move: ShortMove, piece) => {
    this.is_captured(move.from, move.to)
    if (this.chess.move(move)) {
      this.setState({ fen: this.chess.fen() }, () => {
        let move_uci = move.from + move.to
        if ((piece == "wP" || piece == "bP") && (move.to[1] == "1" || move.to[1] == "8")) {
          move_uci = move_uci + "q"
        }
        this.chess_ws.send(JSON.stringify({ type: "move", move: move_uci }))
        this.play_sound(player_move)
        this.last_move(move.from, move.to)
        this.moves_history(move.from + move.to)
        this.in_check()
        this.handleOpponentMove()
        console.log("send ", move_uci)
      })
    }
  };

  handleOpponentMove = () => {
    this.setState({ drag: false })
    this.chess_ws.onmessage = (response) => {
      let msg = JSON.parse(response.data)
      console.log("got ", msg)
      if (msg.type == "move") {
        let move = {
          from: msg.move.slice(0, 2),
          to: msg.move.slice(2, 4),
          promotion: "q",
        }
        this.is_captured(move.from, move.to)
        this.chess.move(move)
        this.setState({ drag: true, fen: this.chess.fen() }, () => {
          this.play_sound(opponent_move)
          this.last_move(move.from, move.to)
          this.moves_history(move.from + move.to)
          this.in_check()
        });
      }
    }
  }

  handle_legal_moves = (square) => {
    if (this.state.drag == false) return  //dont hint other player legal moves
    let moves = this.chess.moves({
      square: square,
    });
    let squareStyles = this.state.squareStyles
    Object.keys(squareStyles).forEach((key) => { // delete old hints if new square was choosen
      if (squareStyles[key] == this.legal_moves_style || squareStyles[key] == this.take_styles) {
        //console.log("delete ", squareStyles[key])
        delete squareStyles[key]
      }
      else if (squareStyles[key] == this.take_and_last_move_styles) {
        squareStyles[key] = this.last_move_to_style
      }
    })
    if (moves.length != 0) { // if square has legal moves add hints
      moves.forEach(move => {
        if (!move.includes("x")) { //if if move isnt a taking move
          squareStyles[move.slice(-2)] = this.legal_moves_style
        }
        else {
          if (squareStyles[move.slice(-2)]) // if there is a last moves styles already add take styles
            squareStyles[move.slice(-2)] = this.take_and_last_move_styles
          else {
            squareStyles[move.slice(-2)] = this.take_styles
          }
        }
      });
    }
    this.setState({ squareStyles: squareStyles })
  }

  last_move = (from, to) => {
    let last_move = { [from]: this.last_move_from_style, [to]: this.last_move_to_style }
    this.setState({ squareStyles: last_move })
  }

  moves_history = (move) => {
    let moves = this.state.moves
    if (moves[moves.length - 1] && moves[moves.length - 1].length != 2) {
      moves[moves.length - 1].push(move)
    }
    else {
      moves.push([move])
    }
    console.log(moves)
  }

  is_captured = (from, to) => {
    if (this.chess.get(to) && from != to) {
      this.play_sound(player_capture)
    }
  }

  in_check = () => {
    if (this.chess.in_check()) {
      this.play_sound(player_check)
      let king_postion = this.get_piece_positions({ type: "k", color: this.chess.turn() })
      let squareStyles = this.state.squareStyles
      squareStyles[king_postion[0]] = this.in_check_style
      this.setState({ squareStyles: squareStyles })
    }
  }

  play_sound = (s) => {
    let sound = new Audio(s)
    sound.volume = 0.2
    sound.play()
  }

  //{ type: 'k', color: 'w'}
  get_piece_positions = (piece) => {
    console.log("bruh")
    return [].concat(...this.chess.board()).map((p, index) => {
      if (p !== null && p.type === piece.type && p.color === piece.color) {
        return index
      }
    }).filter(Number.isInteger).map((piece_index) => {
      const row = 'abcdefgh'[piece_index % 8]
      const column = Math.ceil((64 - piece_index) / 8)
      return row + column
    })
  }

  clear = async () => {
    await (this.chess = new Chess())
    this.setState({ fen: this.chess.fen() })
  }

  componentDidMount() {
    this.connectSocket()
  }


  public render(): JSX.Element {
    if (this.state.status == "connecting") {
      return (
        <h1>
          Connecting...
        </h1>
      )
    }
    else if (this.state.status == "waiting") {
      return (
        <h1>Waiting for opponent...</h1>
      )
    }
    else if (this.state.status == "start") {
      return (
        <div className="game">
          <div className="stats"></div>
          <div className="chess">
            <div className="opponent">
              <h3>{this.state.opponent_name}</h3>
            </div>
            <Chessboard
              width={800}
              draggable={this.state.drag}
              orientation={this.state.orientation}
              position={this.state.fen}
              transitionDuration={0}
              lightSquareStyle={this.light_square_styles}
              darkSquareStyle={this.dark_square_styles}
              squareStyles={this.state.squareStyles}
              onMouseOverSquare={this.handle_legal_moves}
              dropSquareStyle={this.highlight_square_styles}
              onDrop={(move) =>
                this.handleLocalMove({
                  from: move.sourceSquare,
                  to: move.targetSquare,
                  promotion: "q",
                }, move.piece)
              }
            />
            <div className="player">
              <h3>{this.state.player_name}</h3>
            </div>
          </div>
          <div className="info">
            <h4>Moves: </h4>
            {
              this.state.moves ?
                this.state.moves.map((pair, id) => {
                  return (
                    <h4 key={id}>
                      {(id + 1) + ". " + pair[0] + (pair[1] ? ", " + pair[1] : "")}
                    </h4>
                  )
                })
                :
                <></>
            }
          </div>
        </div>
      );
    }
  }
}

export default ChessGame;
