import React from 'react';
import ChessGame from '../components/chess';

interface WebState {
}

class App extends React.Component<{}, WebState> {

  constructor(props) {
    super(props)
  }

  public render(): JSX.Element {
    return (
      <ChessGame />
    );
  }
};


export default App;
