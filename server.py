import asyncio
import websockets
import json
from collections import deque
import chess

def update_elo(winner_elo, loser_elo):
    expected_win = expected_result(winner_elo, loser_elo)
    change_in_elo = 32 * (1-expected_win)
    winner_elo += change_in_elo
    loser_elo -= change_in_elo
    return winner_elo, loser_elo

def expected_result(elo_a, elo_b):
    return 1.0/(1+10**((elo_b - elo_a)/400))

class Client:
	def __init__(self, name, socket, id):
		self.id = id
		self.name = name
		self.websocket = socket
		self.elo = 1200
		self.closed = False

		self.message_handlers = {}

	def add_handler(self, type, handler):
		if type not in self.message_handlers:
			self.message_handlers[type] = [handler]
		else:
			self.message_handlers[type].append(handler)

	async def invoke_handlers(self, req):
		if self.closed:
			return
		for handler in self.message_handlers[req["type"]]:
			await handler(self, req)

	async def run(self):
		try:
			while True:
				req = json.loads(await self.websocket.recv())
				print(req)
				await self.invoke_handlers(req)
		except websockets.exceptions.ConnectionClosed:
			await self.close()

	async def close(self):
		if self.closed:
			return
		self.closed = True
		await self.invoke_handlers({"type": "disconnect"})
		self.message_handlers.clear()

	async def send(self, msg):
		if type(msg) == dict:
			msg = json.dumps(msg)
		print("sending message")
		try:
			await self.websocket.send(msg)
		except Exception as e:
			print(e)
			await self.close()

class Game:
	def __init__(self, player_1, player_2):
		self.player_1 = player_1
		self.player_2 = player_2
		self.board = chess.Board()

	async def broadcast(self, msg):
		await self.player_1.send(msg)
		await self.player_2.send(msg)

	async def prepare_game(self):
		self.player_1.add_handler("move", self.handle_move)
		self.player_2.add_handler("move", self.handle_move)

		self.player_1.add_handler("disconnect", self.handle_disconnect)
		self.player_2.add_handler("disconnect", self.handle_disconnect)

	async def handle_move(self, player, req):
		print("doing move")
		move = req["move"]
		try:
			self.board.push_uci(move)
		except ValueError as e:
			print(e)
			await player.send({"type": "invalid_move"})
			return
		if player == self.player_1:
			print("sending to 2")
			await self.player_2.send(req)
		else:
			print("sending to 1")
			await self.player_1.send(req)

		print(self.board)

	async def handle_disconnect(self, player, req):
		print("player disconnected")
		if player == self.player_1:
			print("sending to 2")
			await self.player_2.send({"type": "forfeit"})
		else:
			print("sending to 1")
			await self.player_1.send({"type": "forfeit"})

	async def start_game(self):
		await self.player_1.send({"type": "begin_game", "color": "white", "opponent_name": self.player_2.name})
		await self.player_2.send({"type": "begin_game", "color": "black", "opponent_name": self.player_1.name})



next_id = 0
clients = {}
matchmaking_clients = deque()
matchmaking_ready_event = asyncio.Event()

async def do_matchmaking():
	while True:
		if len(matchmaking_clients) < 2:
			print("waiting for players...")
			await matchmaking_ready_event.wait()
			matchmaking_ready_event.clear()
			if len(matchmaking_clients) < 2:
				print("not yet")
				continue

		player_1 = matchmaking_clients.popleft()
		player_2 = matchmaking_clients.popleft()

		game = Game(player_1, player_2)
		await game.prepare_game()
		await game.start_game()

async def matchmaker_add_player(player, req):
	matchmaking_clients.append(player)
	matchmaking_ready_event.set()


async def handle_client(cli, path):
	global next_id
	registration_data = json.loads(await cli.recv())
	client = Client(registration_data["name"], cli, next_id)
	next_id += 1
	print(f"Hello, {client.name}#{client.id}")
	await client.send({"type": "registration_result", "success": True, "id": client.id})
	clients[registration_data["name"]] = client
	client.add_handler("look_for_opponent", matchmaker_add_player)
	await client.run()


start_server = websockets.serve(handle_client, "0.0.0.0", 8765)
make_matches = do_matchmaking()

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_until_complete(make_matches)
asyncio.get_event_loop().run_forever()
